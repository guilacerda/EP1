#include <iostream>
#include <string>
#include <fstream>

#include "menu.hpp"
#include "imagem.hpp"
#include "filtronegativo.hpp"
#include "filtropretobranco.hpp"
#include "filtropolarizado.hpp"

using namespace std;

Menu::Menu(){
        cout << "\n**********Filtros para imagem PPM***********" << endl;
        cout << "\n\nEscolha um dos filtros abaixo:" << endl;
        cout << "\n1. Média \n2. Negativo \n3. Preto e Branco \n4. Polarizado \n\n0. Sair " << endl;

        int filtro_requerido;

        cin >> filtro_requerido;
        
        while(filtro_requerido < 0 || filtro_requerido > 4 || filtro_requerido == 1){
                if(filtro_requerido == 1){
                        cout << "\n********************************************************" << endl;
                        cout << "*Filtro ainda não implementado. Aguarde atualizações...*" << endl;
                        cout << "********************************************************" << endl;
                        cout << "\n\nEscolha um dos filtros abaixo: " << endl;
                        cout << "\n1. Média \n2. Negativo \n3. Preto e Branco \n4. Polarizado \n\n0. Sair " << endl;
                        cin >> filtro_requerido;
                }
                else{
                        cout << "\nOpção inválida, escolha um filtro válido: " << endl;
                        cout << "\n1. Média \n2. Negativo \n3. Preto e Branco \n4. Polarizado \n\n0. Sair " << endl;
                        cin >> filtro_requerido;
                }
        } 
        
        FiltroNegativo *filtronegativo = new FiltroNegativo();
        FiltroPolarizado *filtropolarizado = new FiltroPolarizado();
        FiltroPretobranco *filtropretobranco = new FiltroPretobranco();

        ofstream imagem_saida;

        switch(filtro_requerido){
                        
                case 2:
                        filtronegativo->Informacao(imagem_saida);
                        break;
        
                case 3:
                        filtropretobranco->Informacao(imagem_saida);
                        break;
        
                case 4:
                        filtropolarizado->Informacao(imagem_saida);
                        break;
        
                case 0: 
                        break;
        }               


	return;

}

Menu::~Menu(){}

