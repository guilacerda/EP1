#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>

#include "filtronegativo.hpp"

using namespace std;

FiltroNegativo::FiltroNegativo(){
	
	setColor(0);
	setLargura(0);
	setAltura(0);	


	
}

FiltroNegativo::~FiltroNegativo(){} //destrutor

FiltroNegativo::FiltroNegativo(int largura, int altura, int color){

	setColor(color);
	setLargura(largura);
	setAltura(altura);
}

void FiltroNegativo::guarda_Pixels(ofstream &imagem_saida){
	
	int color;
	color = getColor();

	int largura;
	largura = getLargura();

	int altura;
	altura = getAltura();	
	
	for(int l = 0; l < altura; l++){
		for(int m = 0; m < largura; m++){
			matrizR[l][m] = color - matrizR[l][m];
			imagem_saida << matrizR[l][m];
 
                        matrizG[l][m] = color - matrizG[l][m];
                        imagem_saida << matrizG[l][m];

                        matrizB[l][m] = color - matrizB[l][m];
                        imagem_saida << matrizB[l][m];
			
		}
	}
}







