#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include "filtropolarizado.hpp"

using namespace std;

FiltroPolarizado::FiltroPolarizado(){
	
	setColor(0);
	setLargura(0);
	setAltura(0);

}

FiltroPolarizado::~FiltroPolarizado(){} // Método destrutor

FiltroPolarizado::FiltroPolarizado(int color, int largura, int altura){
	
	setColor(color);
	setLargura(largura);
	setAltura(altura);

}

void FiltroPolarizado::guarda_Pixels(ofstream &imagem_saida){
	
	int color;
	color = getColor();

	int largura;
	largura = getLargura();
	
	int altura;
	altura = getAltura();

	for(int k = 0; k < altura; ++k){
    		for(int l = 0; l < largura; ++l){
           		if(matrizR[k][l] < color/2){
               			matrizR[k][l] = 0;
          		}

			else{
		               matrizR[k][l] = color;
          		}

           		if(matrizG[k][l] < color/2){
               			matrizG[k][l] = 0;
           		}

			else{
               			matrizG[k][l] = color;
           		}

           		if(matrizB[k][l] < color/2){
               			matrizG[k][l] = 0;
           		}

			else{
		               matrizB[k][l] = color;
          		}
    	
			imagem_saida << matrizR[k][l];
	 		imagem_saida << matrizG[k][l]; 
			imagem_saida << matrizB[k][l];
		}
	}
}









