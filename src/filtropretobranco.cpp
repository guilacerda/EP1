#include <iostream>
#include <string>
#include <cstdio>

#include "filtropretobranco.hpp"

using namespace std;

FiltroPretobranco::FiltroPretobranco(){
	
	setColor(0);
	setLargura(0);
	setAltura(0);
}

FiltroPretobranco::~FiltroPretobranco(){} // Método Destrutor

FiltroPretobranco::FiltroPretobranco(int largura, int altura, int color){

	setColor(color);
	setLargura(largura);
	setAltura(altura);
}

void FiltroPretobranco::guarda_Pixels(ofstream &imagem_saida){
	
	int altura;
	altura = getAltura();
	
	int largura;
	largura = getLargura();
	
	int color;
	color = getColor();

	int cinza_max;

	for(int i = 0; i < altura; i++){
    		for(int j = 0; j < largura; j++){
          	
		 	cinza_max = (0.299 * matrizR[i][j]) + (0.587 * matrizG[i][j]) + (0.144 * matrizB[i][j]);
			if(color > cinza_max){	
           			matrizR[i][j] = cinza_max;
				imagem_saida << matrizR[i][j];

    		       		matrizG[i][j] = cinza_max;
				imagem_saida << matrizG[i][j];

           			matrizB[i][j] = cinza_max;
				imagem_saida << matrizB[i][j];    		
			}

			else{
				matrizR[i][j] = color;
				imagem_saida << matrizR[i][j];

				matrizG[i][j] = color;
				imagem_saida << matrizG[i][j];

				matrizB[i][j] = color;
				imagem_saida << matrizB[i][j];
			}
		}
	}
}
