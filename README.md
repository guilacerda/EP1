# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**

Obs.: para facilitar o teste desse programa, coloque a imagem ppm na pasta ./doc/ .

## Funcionamento

O funcionamento do programa está descrito a seguir:

* Escolha o tipo de filtro que deseja aplicar.
* O programa pedirá para inserir o nome da imagem que vai ser modificada.
* Logo em seguida, o programa irá pedir o nome da nova imagem, a qual vai ser salva as alterações feitas pelo filtro escolhido.
* Abra o diretório documentos(./doc/), onde a imagem foi salva, e veja o resultado.

## Formato

É importante salientar que esse programa foi desenvolvido para trabalhar com imagens do tipo PPM cujo número mágico é **P6**.
 

