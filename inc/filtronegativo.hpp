#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include <iostream>
#include "imagem.hpp"

class FiltroNegativo: public Imagem {
public:
	FiltroNegativo();
	~FiltroNegativo();

	FiltroNegativo(int largura, int altura, int color);
	
	void guarda_Pixels(ofstream &imagem_saida);

};



#endif
