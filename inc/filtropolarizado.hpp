#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include <iostream>
#include "imagem.hpp"

class FiltroPolarizado: public Imagem{
public:

	FiltroPolarizado();
	~FiltroPolarizado();

	FiltroPolarizado(int color, int largura, int altura);
	
	void guarda_Pixels(ofstream &imagem_saida);

};

#endif
