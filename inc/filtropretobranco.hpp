#ifndef FILTROPRETOBRANCO_HPP
#define FILTROPRETOBRANCO_HPP

#include <iostream>
#include "imagem.hpp"

using namespace std;

class FiltroPretobranco:public Imagem{
public:
	FiltroPretobranco();
	~FiltroPretobranco();

	FiltroPretobranco(int largura, int altura, int color);
	
	void guarda_Pixels(ofstream &imagem_saida);

};







#endif
